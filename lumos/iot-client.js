const app = document.getElementById('app');
const statusBox = document.getElementById('statusBox');

// const url = "http://krishian.tajmiulhasan.com/";
// const url = "https://api.krishian.adilarham.com/";
const url = "http://localhost:8883";
const socket = io(url)

const theButton1 = document.getElementById('theButton1');
const theButton2 = document.getElementById('theButton2');
const theButton3 = document.getElementById('theButton3');
const theButton4 = document.getElementById('theButton4');

[theButton1, theButton2, theButton3, theButton4].forEach(button => {
    button.addEventListener('click', function (e) {
        let theButtonValue = parseInt(e.target.dataset.value);
        console.log("Clicked", theButtonValue);
        fetch(url, {
            method: 'POST',
            body: JSON.stringify({
                btnValue: theButtonValue
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        })
            .then(response => response.json())
            .then(json => {
                console.log(json);
            }).catch(err => {
                console.log(err);
            })
    })
})

let count = 0;
/**
 * The function will render data in a table
 * 
 * @param {object} data The data sent from server 
 * @param {object} status Server status sent from server 
 * @returns {string} html
 */
function render(data, status = null) {
    if (count > 8) {
        app.removeChild(app.lastElementChild)
    }
    count++
    statusBox.innerText = status.message;
    statusBox.style.color = "green";

    let row = document.createElement('tr');
    let values = data.body

    let dataItems = ['Temperature', "ph_value", "humidity", "watertemp", "light", "EC", "Time",];

    let noCell = document.createElement('th');
    noCell.innerText = count;
    row.appendChild(noCell);
    dataItems.forEach(item => {
        let cell = document.createElement('td');
        cell.innerText = values[item];
        row.appendChild(cell);
    });


    console.log(row);
    app.prepend(row);
}
// render html

statusBox.innerText = "Not connected";

socket.on("newInfo", (res) => {
    console.log(res);
    render(res.result, {
        message: res.message,
        statusCode: 200
    });
});

socket.on("onConnect", (res) => {
    console.warn(res);
    statusBox.innerText = res.message;
    statusBox.style.color = "blue";
});

fetch(url)
    .then(response => response.json())
    .then(json => {
        console.log(json);
    }).catch(err => {
        console.log(err);
    })

